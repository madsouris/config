# What to install

### Install Command Line Tool

```shell
xcode-select --install
```



### Install Homebrew

[Brew.sh](https://brew.sh)

---

### Install packages

__Normal pacakges__

```shell
brew install coreutils micro neofetch nodejs vim python
```

__Install Casks__

```shell
brew cask install android-platform-tools imageoptim the-unarchiver appcleaner mark-text ticktick bitwarden microsoft-teams transmission brave-browser tutanota discord rectangle visual-studio-code spotify vlc figma sublime-text firefox telegram
```

__Common used NPM__

```shell
npm install -g @vue/cli sass serve
```

__Install Powerlevel10k__

[GitHub - romkatv/powerlevel10k: A Zsh theme](https://github.com/romkatv/powerlevel10k)

---

### Copy file over

zsh, vim, nano, aliases are the same in Linux folder, use that.
