# Setup Samba server for local file sharing

- Install samba
  - `sudo apt install samba mlocate`
- Verify samba status with `systemctl status smbd`
- Edit config
  - `sudo nano /etc/samba/smb.conf`
- Add below code to the end

```yaml
[sharing]
comment = Samba share directory
path = /home
read only = no
writable = yes
browseable = yes
guest ok = no
valid users = @souris
```

- Allow user with
  - `sudo smbpasswd -a souris`
- Restart smbd
  - `sudo  systemctl restart smbd`
