#!/bin/bash

cp nanorc ~/.nanorc
cp vimrc ~/.vimrc
cp aliases ~/.aliases
# cp zshrc ~/.zshrc

echo "source $HOME/.aliases" >> ~/.zshrc