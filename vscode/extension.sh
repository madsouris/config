#!/bin/bash
code --install-extension antfu.unocss &&
code --install-extension dbaeumer.vscode-eslint &&
code --install-extension eamodio.gitlens &&
code --install-extension esbenp.prettier-vscode &&
code --install-extension formulahendry.auto-rename-tag &&
code --install-extension miguelsolorio.min-theme &&
code --install-extension ms-dotnettools.csharp &&
code --install-extension ritwickdey.LiveServer &&
code --install-extension VisualStudioExptTeam.vscodeintellicode &&
code --install-extension Vue.volar &&
code --install-extension Vue.vscode-typescript-vue-plugin &&
code --install-extension wayou.vscode-icons-mac &&
code --install-extension Zignd.html-css-class-completion
