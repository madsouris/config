## Homebrew

- Debian or Ubuntu `sudo apt-get install build-essential procps curl file git zsh mlocate`
- Fedora - `sudo yum groupinstall 'Development Tools'` - `sudo yum install procps-ng curl file git`
- Arch - `sudo pacman -S git base-devel`
- Install brew `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- Install packages - `brew install gh neofetch micro coreutils node romkatv/powerlevel10k/powerlevel10k romkatv/powerlevel10k/powerlevel10k`
- Install npm pagacke globally - `npm i -g serve prettier`
